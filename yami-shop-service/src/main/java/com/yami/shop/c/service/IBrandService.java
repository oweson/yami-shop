package com.yami.shop.c.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.yami.shop.bean.model.Brand;

import java.util.List;

/**
 * @author oweson
 * @date 2022/2/12 20:39
 */


public interface IBrandService extends IService<Brand> {

    /**
     * 1 根据品牌查名字询品牌
     *
     * @param brandName
     * @return
     */
    Brand getByBrandName(String brandName);

    /**
     * 2 根据id删除品牌，同时删除品牌和分类的关联关系
     *
     * @param id
     */
    void deleteBrandById(Long id);

    /**
     * 3 根据分类id获得品牌列表
     *
     * @param id
     * @return
     */
    List<Brand> listByCategoryId(Long id);
}
