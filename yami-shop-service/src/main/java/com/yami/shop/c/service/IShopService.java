package com.yami.shop.c.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.yami.shop.bean.model.ShopDetail;

/**
 * @author oweson
 * @date 2022/2/17 21:47
 */


public interface IShopService extends IService<ShopDetail> {

    /**
     * 1 更新店铺信息
     *
     * @param shopDetail
     * @param dbShopDetail
     */
    void updateShopDetail(ShopDetail shopDetail, ShopDetail dbShopDetail);

    /**
     * 2 删除店铺
     *
     * @param id
     */
    void deleteShopDetailByShopId(Long id);

    /**
     * 3 根据店铺id获得店铺信息
     *
     * @param id
     * @return
     */

    ShopDetail getShopDetailByShopId(Long id);

    /**
     * 4 根据店铺id删除缓存
     *
     * @param id
     */
    void removeShopDetailCacheByShopId(Long id);

}
