package com.yami.shop.c.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.yami.shop.bean.model.UserAddr;

/**
 * @author oweson
 * @date 2022/2/14 21:54
 */


public interface IUserAddressService extends IService<UserAddr> {

    /**
     * 1 获得用户的默认收货地址
     *
     * @param userId
     * @return
     */
    UserAddr getDefaultUserAddr(String userId);

    /**
     * 2 更新用户的默认收货地址
     *
     * @param addrId
     * @param userId
     */
    void updateDefaultUserAddr(Long addrId, String userId);

    /**
     * 3 移除用户的收货地址
     *
     * @param addrId
     * @param userId
     */
    void removeUserAddrByUserId(Long addrId, String userId);

    /**
     * 4 根据用户id获得用户的收货地址
     *
     * @param addrId
     * @param userId
     * @return
     */
    UserAddr getUserAddrByUserId(Long addrId, String userId);
}
