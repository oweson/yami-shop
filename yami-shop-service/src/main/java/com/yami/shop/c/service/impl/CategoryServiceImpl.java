package com.yami.shop.c.service.impl;

import cn.hutool.core.collection.CollectionUtil;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.yami.shop.bean.app.dto.CategoryDto;
import com.yami.shop.bean.model.Category;
import com.yami.shop.c.service.ICategoryService;
import com.yami.shop.dao.CategoryBrandMapper;
import com.yami.shop.dao.CategoryMapper;
import com.yami.shop.dao.CategoryPropMapper;
import com.yami.shop.service.AttachFileService;
import ma.glasnost.orika.MapperFacade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collections;
import java.util.Date;
import java.util.List;

/**
 * @author oweson
 * @date 2022/2/21 22:39
 */

@Service
public class CategoryServiceImpl extends ServiceImpl<CategoryMapper, Category> implements ICategoryService {
    @Autowired
    private CategoryMapper categoryMapper;

    @Autowired
    private CategoryBrandMapper categoryBrandMapper;

    @Autowired
    private CategoryPropMapper categoryPropMapper;
    @Autowired

    private AttachFileService attachFileService;
    @Autowired
    private MapperFacade mapperFacade;

    @Override
    public List<Category> listByParentId(Long parentId) {
        return categoryMapper.listByParentId(parentId);
    }

    @Override
    public List<Category> tableCategory(Long shopId) {
        return categoryMapper.tableCategory(shopId);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void saveCategory(Category category) {
        // 保存分类信息
        category.setRecTime(new Date());
        categoryMapper.insert(category);
        // todo 保存品牌和属性
        insertBrandAndAttributes(category);


    }

    @Override
    public void deleteCategory(Long category) {

    }

    @Override
    public List<Category> treeSelect(Long shopId, int grade) {
        return null;
    }

    @Override
    public List<CategoryDto> listCategoryDtoByShopId(Long shopId) {
        return null;
    }

    @Override
    public void updateCategory(Category category) {

    }

    @Transactional(rollbackFor = Exception.class)
    private void deleteBrandAndAttributes(Long categoryId) {
        // 删除所有分类所关联的品牌
        categoryBrandMapper.deleteByCategoryId(categoryId);
        // 删除所有分类所关联的参数
        categoryPropMapper.deleteByCategoryId(categoryId);
    }

    private void insertBrandAndAttributes(Category category) {
        // 保存分类与参数
        if (CollectionUtil.isNotEmpty(category.getAttributeIds())) {
            categoryPropMapper.insertCategoryProp(category.getCategoryId(), category.getAttributeIds());
        }
        // 2 保存分类与品牌信息
        if (CollectionUtil.isNotEmpty(category.getAttributeIds())) {
            categoryBrandMapper.insertCategoryBrand(category.getCategoryId(), category.getBrandIds());
        }
    }
}
