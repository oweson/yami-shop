package com.yami.shop.c.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.yami.shop.bean.model.ShopDetail;
import com.yami.shop.c.service.IShopService;
import com.yami.shop.dao.AttachFileMapper;
import com.yami.shop.dao.ShopDetailMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author oweson
 * @date 2022/2/17 21:50
 */

@Service
public class ShopServiceImpl extends ServiceImpl<ShopDetailMapper, ShopDetail> implements IShopService {

    @Autowired
    private ShopDetailMapper shopDetailMapper;

    @Autowired
    private AttachFileMapper attachFileMapper;

    @Override
    @Transactional(rollbackFor = Exception.class)
    @CacheEvict(cacheNames = "shop_detail", key = "#shopDetail.shopId")
    public void updateShopDetail(ShopDetail shopDetail, ShopDetail dbShopDetail) {
        shopDetailMapper.updateById(shopDetail);

    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    @CacheEvict(cacheNames = "shop_detail", key = "#shopId")
    public void deleteShopDetailByShopId(Long id) {
        shopDetailMapper.deleteById(id);

    }

    @Override
    @Cacheable(cacheNames = "shop_detail", key = "#shopId")
    public ShopDetail getShopDetailByShopId(Long id) {
        return shopDetailMapper.selectById(id);
    }

    @Override
    @CacheEvict(cacheNames = "shop_detail", key = "#shopId")
    public void removeShopDetailCacheByShopId(Long id) {

    }
}
