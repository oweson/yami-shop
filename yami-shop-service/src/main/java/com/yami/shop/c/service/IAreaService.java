package com.yami.shop.c.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.yami.shop.bean.model.Area;

import java.util.List;

/**
 * @author oweson
 * @date 2022/2/16 21:50
 */


public interface IAreaService extends IService<Area> {
    /**
     * 1 根据pid查找地址接口
     *
     * @param pid
     * @return
     */

    List<Area> listByPid(Long pid);

    /**
     * 2 通过pid清除缓存
     *
     * @param pid
     */
    void removeAreaCacheByParentId(Long pid);
}
