package com.yami.shop.c.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.yami.shop.bean.model.UserAddr;
import com.yami.shop.c.service.IUserAddressService;
import com.yami.shop.common.exception.YamiShopBindException;
import com.yami.shop.dao.UserAddrMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author oweson
 * @date 2022/2/14 21:59
 */

@Service
public class UserAddressImpl extends ServiceImpl<UserAddrMapper, UserAddr> implements IUserAddressService {

    @Autowired
    private UserAddrMapper userAddrMapper;

    @Override
    public UserAddr getDefaultUserAddr(String userId) {
        return userAddrMapper.getDefaultUserAddr(userId);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void updateDefaultUserAddr(Long addrId, String userId) {
        userAddrMapper.removeDefaultUserAddr(userId);
        int effectCount = userAddrMapper.setDefaultUserAddr(addrId, userId);
        if (effectCount == 0) {
            throw new YamiShopBindException("无法修改用户默认收货地址，请稍后重试！");
        }


    }

    @Override
    @CacheEvict(cacheNames = "UserAddrDto", key = "#userId+':'+#addrId")
    public void removeUserAddrByUserId(Long addrId, String userId) {

    }

    @Override
    @Cacheable(cacheNames = "UserAddrDto", key = "#userId+':'+#addrId")
    public UserAddr getUserAddrByUserId(Long addrId, String userId) {
        // todo
        if (addrId == 0) {
            return userAddrMapper.getDefaultUserAddr(userId);
        }
        return userAddrMapper.getUserAddrByUserIdAndAddrId(userId, addrId);
    }
}
