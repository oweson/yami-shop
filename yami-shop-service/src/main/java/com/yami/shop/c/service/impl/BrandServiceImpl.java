package com.yami.shop.c.service.impl;

import com.baomidou.mybatisplus.extension.service.IService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.yami.shop.bean.model.Brand;
import com.yami.shop.c.service.IBrandService;
import com.yami.shop.dao.BrandMapper;
import com.yami.shop.dao.CategoryBrandMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author oweson
 * @date 2022/2/12 20:43
 */

@Service
public class BrandServiceImpl extends ServiceImpl<BrandMapper, Brand> implements IBrandService {
    @Resource
    private BrandMapper brandMapper;
    @Resource
    private CategoryBrandMapper categoryBrandMapper;

    @Override
    public Brand getByBrandName(String brandName) {
        return brandMapper.getByBrandName(brandName);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void deleteBrandById(Long id) {
        brandMapper.deleteById(id);
        categoryBrandMapper.deleteByBrandId(id);

    }

    @Override
    public List<Brand> listByCategoryId(Long id) {
        return brandMapper.listByCategoryId(id);
    }
}
