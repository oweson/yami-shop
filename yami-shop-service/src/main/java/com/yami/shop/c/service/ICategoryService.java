package com.yami.shop.c.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.yami.shop.bean.app.dto.CategoryDto;
import com.yami.shop.bean.model.Category;

import java.util.List;

/**
 * @author oweson
 * @date 2022/2/21 22:32
 */


public interface ICategoryService extends IService<Category> {
    /**
     * 1 根据parentId获得分类
     *
     * @param parentId 0 一级分类
     * @return
     */
    List<Category> listByParentId(Long parentId);

    /**
     * 2 获取用于页面表单展现的category列表，根据seq排序
     *
     * @param shopId
     * @return
     */
    List<Category> tableCategory(Long shopId);

    /**
     * 3 保存分类、品牌、参数
     *
     * @param category
     */
    void saveCategory(Category category);


    /**
     * 4 删除分类、品牌、参数 以及分类对应的图片
     *
     * @param category
     */
    void deleteCategory(Long category);

    /**
     * 5 根据店铺id和层级，获取商品分类树
     *
     * @param shopId
     * @param grade
     * @return
     */
    List<Category> treeSelect(Long shopId, int grade);


    /**
     * 修改分类、品牌、参数
     *
     * @param category
     */
    void updateCategory(Category category);

    List<CategoryDto> listCategoryDtoByShopId(Long shopId);

}
