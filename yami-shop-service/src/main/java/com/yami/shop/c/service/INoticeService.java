package com.yami.shop.c.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.yami.shop.bean.app.dto.NoticeDto;
import com.yami.shop.bean.model.Notice;

import java.util.List;

/**
 * @author oweson
 * @date 2022/2/12 17:13
 */


public interface INoticeService extends IService<Notice> {
    /**
     * 1 查找所有的通知
     *
     * @return
     */
    List<Notice> listNotice();

    /**
     * 2 清除通知列表
     */
    void removeNoticeList();

    /**
     * 3 通知列表分页查询
     *
     * @param page
     * @return
     */

    Page<NoticeDto> pageNotice(Page<NoticeDto> page);

    /**
     * 4 根据公告id删除公告通知
     *
     * @param id
     * @return
     */
    Integer removeNoticeById(Long id);

    /**
     * 5 根据公告id查询公告
     *
     * @param id
     * @return
     */
    Notice getNoticeById(Long id);
}
