package com.yami.shop.admin.c.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.yami.shop.bean.model.Area;
import com.yami.shop.c.service.IAreaService;
import com.yami.shop.common.util.PageParam;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * @author oweson
 * @date 2022/2/16 21:49
 */

@RestController
@RequestMapping("/c/admin/area")
@AllArgsConstructor
public class AreaController {
    private final IAreaService iAreaService;

    /**
     * 分页获取 ok
     */
    @GetMapping("/page")
    @PreAuthorize("@pms.hasPermission('admin:area:page')")
    public ResponseEntity<IPage<Area>> page(Area area, PageParam<Area> page) {
        PageParam<Area> sysUserPage = iAreaService.page(page, new LambdaQueryWrapper<Area>());
        return ResponseEntity.ok(sysUserPage);
    }

    /**
     * 获取省市 ok
     */
    @GetMapping("/list")
    @PreAuthorize("@pms.hasPermission('admin:area:list')")
    public ResponseEntity<List<Area>> list(Area area) {
        List<Area> areas = iAreaService.list(new LambdaQueryWrapper<Area>()
                .eq(area.getAreaName() != null, Area::getAreaName, area.getAreaName()));

        return ResponseEntity.ok(areas);
    }

    /**
     * 通过父级id获取区域列表
     */
    @GetMapping("/listByPid")
    public ResponseEntity<List<Area>> listByPid(Long pid) {
        return ResponseEntity.ok(iAreaService.listByPid(pid));
    }

    /**
     * 获得信息
     *
     * @param id
     * @return
     */
    @GetMapping("/info/{id}")
    @PreAuthorize("@pms.hasPermission('admin:area:info')")
    public ResponseEntity<Area> info(@PathVariable("id") long id) {
        return ResponseEntity.ok(iAreaService.getById(id));
    }

    /**
     * 保存 ok
     */
    @PostMapping
    @PreAuthorize("@pms.hasPermission('admin:area:save')")
    public ResponseEntity<Boolean> save(@Valid @RequestBody Area area) {
        if (area.getParentId() != null) {
            Area parentArea = iAreaService.getById(area.getParentId());
            // 层级
            area.setLevel(parentArea.getLevel() + 1);
            iAreaService.removeAreaCacheByParentId(area.getParentId());
        }
        iAreaService.save(area);
        return ResponseEntity.ok().build();
    }

    /**
     * 修改 ok
     * todo 更换pid未考虑！一般只是更新名字等信息！
     */
    @PutMapping
    @PreAuthorize("@pms.hasPermission('admin:area:update')")
    public ResponseEntity<Boolean> update(@Valid @RequestBody Area area) {
       /* if (area.getParentId() != null) {
            Area parentArea = iAreaService.getById(area.getParentId());
            area.setLevel(parentArea.getLevel() + 1);

        }*/
        // todo 考虑多了！考虑错了！
        iAreaService.removeAreaCacheByParentId(area.getParentId());

        iAreaService.updateById(area);

        return ResponseEntity.ok().build();
    }

    /**
     * 删除,单位删除 todo
     */
    @DeleteMapping("/{id}")
    @PreAuthorize("@pms.hasPermission('admin:area:delete')")
    public ResponseEntity<Boolean> remove(@PathVariable("id") long id) {
        Area dbArea = iAreaService.getById(id);
        if (dbArea.getParentId() != null) {
            iAreaService.removeAreaCacheByParentId(dbArea.getParentId());
        }
        iAreaService.removeById(id);
        return ResponseEntity.ok().build();
    }

}
