package com.yami.shop.admin.c.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.yami.shop.bean.model.ShopDetail;
import com.yami.shop.bean.param.ShopDetailParam;
import com.yami.shop.c.service.IShopService;
import com.yami.shop.common.util.PageParam;
import com.yami.shop.security.util.SecurityUtils;
import ma.glasnost.orika.MapperFacade;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author oweson
 * @date 2022/2/17 21:46
 */

@RestController
@RequestMapping("/c/shop/shopDetail")
public class ShopDetailController {
    @Autowired
    private IShopService iShopService;
    @Autowired
    private MapperFacade mapperFacade;

    /**
     * 1 修改分销开关
     *
     * @param isDistribution
     * @return
     */
    @PutMapping("/isDistribution")
    public ResponseEntity<Boolean> updateIsDistribution(@RequestParam Integer isDistribution) {
        ShopDetail shopDetail = new ShopDetail();
        shopDetail.setShopId(SecurityUtils.getSysUser().getShopId());
        shopDetail.setIsDistribution(isDistribution);
        iShopService.updateById(shopDetail);
        // 更新完成后删除缓存
        iShopService.removeShopDetailCacheByShopId(shopDetail.getShopId());

        return ResponseEntity.ok().build();
    }

    /**
     * 2 获取信息
     */
    @GetMapping("/info")
    @PreAuthorize("@pms.hasPermission('shop:shopDetail:info')")
    public ResponseEntity<ShopDetail> info() {
        return ResponseEntity.ok(iShopService.getShopDetailByShopId(SecurityUtils.getSysUser().getShopId()));
    }

    /**
     * 3 分页获取
     */
    @GetMapping("/page")
    @PreAuthorize("@pms.hasPermission('shop:shopDetail:page')")
    public ResponseEntity<IPage<ShopDetail>> page(ShopDetail shopDetail, PageParam<ShopDetail> page) {
        PageParam<ShopDetail> shopDetails = iShopService.page(page, new LambdaQueryWrapper<ShopDetail>()
                .like(StringUtils.isNotBlank(shopDetail.getShopName()), ShopDetail::getShopName, shopDetail.getShopName())
                .orderByDesc(ShopDetail::getShopId));

        return ResponseEntity.ok(shopDetails);

    }

    /**
     * 4 获取信息
     */
    @GetMapping("/info/{shopId}")
    @PreAuthorize("@pms.hasPermission('shop:shopDetail:info')")
    public ResponseEntity<ShopDetail> info(@PathVariable("shopId") Long shopId) {
        ShopDetail shopDetail = iShopService.getShopDetailByShopId(shopId);
        return ResponseEntity.ok(shopDetail);
    }

    /**
     * 5 删除
     */
    @DeleteMapping("/{id}")
    @PreAuthorize("@pms.hasPermission('shop:shopDetail:delete')")
    public ResponseEntity<Boolean> delete(@PathVariable long id) {
        iShopService.deleteShopDetailByShopId(id);
        return ResponseEntity.ok().build();
    }

    /**
     * 6 修改
     */
    @PutMapping
    @PreAuthorize("@pms.hasPermission('shop:shopDetail:update')")
    public ResponseEntity<Boolean> update(@Valid ShopDetailParam shopDetailParam) {
        ShopDetail shopDetail = iShopService.getShopDetailByShopId(shopDetailParam.getShopId());
        ShopDetail detail = mapperFacade.map(shopDetail, ShopDetail.class);
        iShopService.updateShopDetail(detail, shopDetail);
        return ResponseEntity.ok().build();


    }

    /**
     * 7 更新店铺状态
     */
    @PutMapping("/shopStatus")
    @PreAuthorize("@pms.hasPermission('shop:shopDetail:shopStatus')")

    public ResponseEntity<Boolean> updateShopStatus(@RequestParam long shopId, @RequestParam Integer shopStatus) {

        ShopDetail shopDetail = new ShopDetail();
        shopDetail.setShopId(shopId);
        shopDetail.setShopStatus(shopStatus);
        iShopService.updateById(shopDetail);
        iShopService.removeShopDetailCacheByShopId(shopId);
        return ResponseEntity.ok().build();
    }

    /**
     * 8 获取所有的店铺名称
     */
    @GetMapping("/listShopName")
    public ResponseEntity<List<ShopDetail>> lishShopName() {
        List<ShopDetail> shopDetails = iShopService.list().stream().map(dbShopDetail -> {
            ShopDetail shopDetail = new ShopDetail();
            shopDetail.setShopId(dbShopDetail.getShopId());
            shopDetail.setShopName(dbShopDetail.getShopName());
            return shopDetail;
        }).collect(Collectors.toList());

        return ResponseEntity.ok(shopDetails);
    }

    /**
     * 9 保存
     */
    @PostMapping
    @PreAuthorize("@pms.hasPermission('shop:shopDetail:save')")
    public ResponseEntity<Boolean> save(@Valid ShopDetailParam shopDetailParam) {
        ShopDetail shopDetail = mapperFacade.map(shopDetailParam, ShopDetail.class);
        shopDetail.setCreateTime(new Date());
        shopDetail.setShopStatus(1);
        iShopService.save(shopDetail);
        return ResponseEntity.ok().build();
    }


}
