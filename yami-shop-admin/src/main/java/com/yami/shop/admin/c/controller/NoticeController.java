package com.yami.shop.admin.c.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.yami.shop.bean.model.Notice;
import com.yami.shop.c.service.INoticeService;
import com.yami.shop.common.annotation.SysLog;
import com.yami.shop.common.util.PageParam;
import com.yami.shop.security.util.SecurityUtils;
import lombok.AllArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Date;

/**
 * @author oweson
 * @date 2022/2/12 17:11
 */

@RestController
@AllArgsConstructor
@RequestMapping("/c/shop/notice")
public class NoticeController {
    private final INoticeService iNoticeService;

    /**
     * 1 分页查询
     *
     * @param page
     * @param notice
     * @return
     */
    @GetMapping("/page")
    public ResponseEntity<IPage<Notice>> getNoticePage(PageParam<Notice> page, Notice notice) {
        IPage<Notice> noticeIPage = iNoticeService.page(page, new LambdaQueryWrapper<Notice>()
                .eq(notice.getStatus() != null, Notice::getStatus, notice.getStatus())
                .eq(notice.getIsTop() != null, Notice::getIsTop, notice.getIsTop())
                .like(StringUtils.isNotEmpty(notice.getTitle()), Notice::getTitle, notice.getTitle()));
        return ResponseEntity.ok(noticeIPage);
    }

    /**
     * 2 根据公共id查询公告
     *
     * @param id
     * @return
     */
    @GetMapping("info/{id}")
    public ResponseEntity<Notice> getById(@PathVariable("id") Long id) {

        return ResponseEntity.ok(iNoticeService.getById(id));
    }


    /**
     * 3 新增公告管理
     *
     * @param notice
     * @return
     */
    @SysLog("新增公告管理")
    @PostMapping
    @PreAuthorize("@pms.hasPermission('shop:notice:save')")
    public ResponseEntity<Boolean> save(@RequestBody @Valid Notice notice) {
        notice.setShopId(SecurityUtils.getSysUser().getShopId());
        if (notice.getStatus() == 1) {
            notice.setPublishTime(new Date());
        }
        notice.setUpdateTime(new Date());
        // 删除缓存
        iNoticeService.removeNoticeList();
        return ResponseEntity.ok(iNoticeService.save(notice));
    }

    /**
     * 4 修改公告
     *
     * @param notice
     * @return
     */
    @SysLog("修改公告")
    @PutMapping
    @PreAuthorize("@pms.hasPermission('shop:notice:update')")
    public ResponseEntity<Boolean> updateById(@RequestBody @Valid Notice notice) {
        Notice oldNotice = iNoticeService.getNoticeById(notice.getId());
        // 发布时间判断！
        if (oldNotice.getStatus() == 0 && notice.getStatus() == 1) {
            notice.setPublishTime(new Date());
        }
        notice.setUpdateTime(new Date());
        iNoticeService.removeNoticeList();
        iNoticeService.removeNoticeById(notice.getId());
        return ResponseEntity.ok(iNoticeService.updateById(notice));
    }

    /**
     * 5 删除公告
     *
     * @param id
     * @return
     */
    @SysLog("删除公告")
    @DeleteMapping("/{id}")
    @PreAuthorize("@pms.hasPermission('shop:notice:delete')")
    public ResponseEntity<Boolean> removeById(@PathVariable("id") Long id) {
        iNoticeService.removeNoticeList();
        // 缓存删除
        iNoticeService.removeNoticeById(id);

        return ResponseEntity.ok(iNoticeService.removeById(id));
    }


}
