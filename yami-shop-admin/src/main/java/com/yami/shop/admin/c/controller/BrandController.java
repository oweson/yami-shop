package com.yami.shop.admin.c.controller;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.yami.shop.bean.model.Brand;
import com.yami.shop.c.service.IBrandService;
import com.yami.shop.common.exception.YamiShopBindException;
import com.yami.shop.common.util.PageParam;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.validation.Valid;
import java.util.Objects;

/**
 * @author oweson
 * @date 2022/2/12 20:38
 */

@RestController
public class BrandController {
    @Resource
    private IBrandService iBrandService;

    /**
     * 1 根据id获得品牌信息
     *
     * @param id
     * @return
     */
    @GetMapping("/info/{id}")
    public ResponseEntity<Brand> getBrand(@PathVariable("id") Long id) {
        return ResponseEntity.ok(iBrandService.getById(id));
    }

    /**
     * 2 保存
     *
     * @param brand
     * @return
     */
    @PostMapping
    @PreAuthorize("@pms.hasPermission('admin:brand:save')")
    public ResponseEntity<Void> save(@Valid Brand brand) {

        if (iBrandService.getByBrandName(brand.getBrandName()) != null) {
            throw new YamiShopBindException("该品牌名称已经存在");
        }
        iBrandService.save(brand);
        return ResponseEntity.ok().build();
    }

    /**
     * 3 品牌更新
     *
     * @param brand
     * @return
     */
    @PutMapping
    @PreAuthorize("@pms.hasPermission('admin:brand:update')")
    public ResponseEntity<Void> updateBrand(@Valid Brand brand) {
        Brand dbBrand = iBrandService.getByBrandName(brand.getBrandName());
        if (dbBrand != null && !Objects.equals(dbBrand.getBrandId(), brand.getBrandId())) {
            throw new YamiShopBindException("该品牌名称已经存在");
        }
        iBrandService.updateById(brand);
        return ResponseEntity.ok().build();
    }

    /**
     * 4 删除
     *
     * @param id
     * @return
     */
    @DeleteMapping("/{id}")
    public ResponseEntity<Void> delete(@PathVariable("id") Long id) {
        iBrandService.deleteBrandById(id);
        return ResponseEntity.ok().build();
    }

    /**
     * 5 分页查找
     *
     * @param brand
     * @param page
     * @return
     */
    public ResponseEntity<IPage<Brand>> page(Brand brand, PageParam<Brand> page) {

        IPage<Brand> brandIPage = iBrandService.page(page, new LambdaQueryWrapper<Brand>()
                .like(StrUtil.isNotBlank(brand.getBrandName()), Brand::getBrandName, brand.getBrandName()));
        return ResponseEntity.ok(brandIPage);
    }

}
