package com.yami.shop.admin.c.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.yami.shop.bean.model.UserAddr;
import com.yami.shop.c.service.IUserAddressService;
import com.yami.shop.common.annotation.SysLog;
import com.yami.shop.common.util.PageParam;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * @author oweson
 * @date 2022/2/14 21:53
 */

@RestController
@AllArgsConstructor
@RequestMapping("/c/user/address")
public class UserAddressController {
    private final IUserAddressService iUserAddressService;

    @GetMapping("/page")
    public ResponseEntity<IPage<UserAddr>> page(PageParam page, UserAddr userAddr) {
        return ResponseEntity.ok(iUserAddressService.page(page, new LambdaQueryWrapper<>()));

    }

    @GetMapping("/info/{addrId}")
    public ResponseEntity<UserAddr> getById(@PathVariable("addrId") Long addrId) {
        return ResponseEntity.ok(iUserAddressService.getById(addrId));
    }

    @PostMapping
    @SysLog("新增用户地址管理")
    @PreAuthorize("@pms.hasPermission('user:addr:save')")
    public ResponseEntity<Boolean> save(@RequestBody @Valid UserAddr userAddr) {
        return ResponseEntity.ok(iUserAddressService.save(userAddr));
    }

    @PutMapping
    @SysLog("修改用户地址管理")
    @PreAuthorize("@pms.hasPermission('user:addr:update')")

    public ResponseEntity<Boolean> updateById(@RequestBody @Valid UserAddr userAddr) {
        return ResponseEntity.ok(iUserAddressService.updateById(userAddr));
    }

    @DeleteMapping
    @SysLog("删除用户地址管理")
    @PreAuthorize("@pms.hasPermission('user:addr:delete')")

    public ResponseEntity<Boolean> remove(@PathVariable("addrId") Long addrId) {
        return ResponseEntity.ok(iUserAddressService.removeById(addrId));
    }

}
